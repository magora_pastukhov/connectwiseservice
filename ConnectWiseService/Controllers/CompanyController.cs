﻿using System.Web.Http;
using Autofac;
using ConnectWiseService.Filters;
using ConnectWiseService.Logic.Abstract;
using ConnectWiseService.Models;
using ConnectWiseService.Models.Company;
using ConnectWiseService.Models.Helpers;

namespace ConnectWiseService.Controllers
{
    [BasicHttpAuthorize]
    public class CompanyController : ApiController
    {
        [Route("api/company/companies")]
        public Company[] Get([FromUri]FilterModel filter = null)
        {
            var helper = IoC.Instance.Resolve<ICompanyHelper>();
            return helper.GetCompanies(filter);
        }
    }
}