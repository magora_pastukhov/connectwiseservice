﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ConnectWiseService.Filters
{
    public class BasicHttpAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            IEnumerable<string> tokens;
            actionContext.Request.Headers.TryGetValues(ConfigurationManager.AppSettings["AuthHeaderName"], out tokens);
            if (tokens == null || !tokens.Any())
            {
                return false;
            }

            return AuthenticateUser(tokens.First());
        }

        private bool AuthenticateUser(string token)
        {
            try
            {
                var credentials = token.Replace("Basic", string.Empty).Trim();
                var encoding = Encoding.GetEncoding("utf-8");
                credentials = encoding.GetString(Convert.FromBase64String(credentials));
                int separator = credentials.IndexOf(":", StringComparison.Ordinal);
                var companyAndPublicKey = credentials.Substring(0, separator);
                var publicSeparator = companyAndPublicKey.IndexOf("+", StringComparison.Ordinal);
                var companyName = companyAndPublicKey.Substring(0, publicSeparator);
                var publicKey = companyAndPublicKey.Substring(publicSeparator + 1);
                var privateKey = credentials.Substring(separator + 1);

                return ConfigurationManager.AppSettings["CompanyName"] == companyName
                       && ConfigurationManager.AppSettings["PublicKey"] == publicKey
                       && ConfigurationManager.AppSettings["PrivateKey"] == privateKey;
            }
            catch
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
}