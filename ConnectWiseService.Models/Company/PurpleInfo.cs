using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class PurpleInfo
    {
        [JsonProperty("type_href")]
        public string TypeHref { get; set; }
    }
}