using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class BillingTerms
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}