using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class BillingSite
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("_info")]
        public CunningInfo Info { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}