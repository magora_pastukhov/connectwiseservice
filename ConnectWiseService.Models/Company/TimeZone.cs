using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class TimeZone
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("_info")]
        public FluffyInfo Info { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}