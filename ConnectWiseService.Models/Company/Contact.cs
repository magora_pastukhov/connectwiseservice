using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class Contact
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("_info")]
        public AmbitiousInfo Info { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}