using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class BillToCompany
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("identifier")]
        public string Identifier { get; set; }

        [JsonProperty("_info")]
        public StickyInfo Info { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}