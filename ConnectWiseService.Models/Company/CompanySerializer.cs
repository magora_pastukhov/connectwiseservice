using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public static class CompanySerializer
    {
        public static string ToJson(this Company[] self) => JsonConvert.SerializeObject(self, CompanyConverter.Settings);
    }
}