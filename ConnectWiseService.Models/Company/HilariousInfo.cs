using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class HilariousInfo
    {
        [JsonProperty("activities_href")]
        public string ActivitiesHref { get; set; }

        [JsonProperty("agreements_href")]
        public string AgreementsHref { get; set; }

        [JsonProperty("configurations_href")]
        public string ConfigurationsHref { get; set; }

        [JsonProperty("contacts_href")]
        public string ContactsHref { get; set; }

        [JsonProperty("dateEntered")]
        public string DateEntered { get; set; }

        [JsonProperty("documents_href")]
        public string DocumentsHref { get; set; }

        [JsonProperty("enteredBy")]
        public string EnteredBy { get; set; }

        [JsonProperty("lastUpdated")]
        public string LastUpdated { get; set; }

        [JsonProperty("notes_href")]
        public string NotesHref { get; set; }

        [JsonProperty("opportunities_href")]
        public string OpportunitiesHref { get; set; }

        [JsonProperty("orders_href")]
        public string OrdersHref { get; set; }

        [JsonProperty("projects_href")]
        public string ProjectsHref { get; set; }

        [JsonProperty("reports_href")]
        public string ReportsHref { get; set; }

        [JsonProperty("sites_href")]
        public string SitesHref { get; set; }

        [JsonProperty("teams_href")]
        public string TeamsHref { get; set; }

        [JsonProperty("tickets_href")]
        public string TicketsHref { get; set; }

        [JsonProperty("updatedBy")]
        public string UpdatedBy { get; set; }
    }
}