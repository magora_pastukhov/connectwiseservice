using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class FluffyInfo
    {
        [JsonProperty("timeZoneSetup_href")]
        public string TimeZoneSetupHref { get; set; }
    }
}