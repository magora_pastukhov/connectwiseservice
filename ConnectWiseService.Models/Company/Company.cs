﻿namespace ConnectWiseService.Models.Company
{
    using Newtonsoft.Json;

    public partial class Company
    {
        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("addressLine1")]
        public string AddressLine1 { get; set; }

        [JsonProperty("addressLine2")]
        public string AddressLine2 { get; set; }

        [JsonProperty("annualRevenue")]
        public long AnnualRevenue { get; set; }

        [JsonProperty("billToCompany")]
        public BillToCompany BillToCompany { get; set; }

        [JsonProperty("billingContact")]
        public Contact BillingContact { get; set; }

        [JsonProperty("billingSite")]
        public BillingSite BillingSite { get; set; }

        [JsonProperty("billingTerms")]
        public BillingTerms BillingTerms { get; set; }

        [JsonProperty("calendarId")]
        public long? CalendarId { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public BillingTerms Country { get; set; }

        [JsonProperty("currency")]
        public Currency Currency { get; set; }

        [JsonProperty("dateAcquired")]
        public string DateAcquired { get; set; }

        [JsonProperty("dateDeleted")]
        public string DateDeleted { get; set; }

        [JsonProperty("defaultContact")]
        public Contact DefaultContact { get; set; }

        [JsonProperty("deletedBy")]
        public string DeletedBy { get; set; }

        [JsonProperty("deletedFlag")]
        public bool DeletedFlag { get; set; }

        [JsonProperty("faxNumber")]
        public string FaxNumber { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("identifier")]
        public string Identifier { get; set; }

        [JsonProperty("_info")]
        public HilariousInfo Info { get; set; }

        [JsonProperty("invoiceDeliveryMethod")]
        public BillingTerms InvoiceDeliveryMethod { get; set; }

        [JsonProperty("invoiceToEmailAddress")]
        public string InvoiceToEmailAddress { get; set; }

        [JsonProperty("leadFlag")]
        public bool LeadFlag { get; set; }

        [JsonProperty("marketId")]
        public long? MarketId { get; set; }

        [JsonProperty("mobileGuid")]
        public string MobileGuid { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("sicCode")]
        public BillingTerms SicCode { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("taxCode")]
        public TaxCode TaxCode { get; set; }

        [JsonProperty("taxIdentifier")]
        public string TaxIdentifier { get; set; }

        [JsonProperty("territoryId")]
        public long TerritoryId { get; set; }

        [JsonProperty("territoryManager")]
        public BillToCompany TerritoryManager { get; set; }

        [JsonProperty("timeZone")]
        public TimeZone TimeZone { get; set; }

        [JsonProperty("type")]
        public PurpleType Type { get; set; }

        [JsonProperty("unsubscribeFlag")]
        public bool UnsubscribeFlag { get; set; }

        [JsonProperty("userDefinedField5")]
        public string UserDefinedField5 { get; set; }

        [JsonProperty("vendorIdentifier")]
        public string VendorIdentifier { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }
    }

    public partial class Company
    {
        public static Company[] FromJson(string json) => JsonConvert.DeserializeObject<Company[]>(json, CompanyConverter.Settings);
    }
}