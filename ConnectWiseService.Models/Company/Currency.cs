using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class Currency
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("_info")]
        public IndecentInfo Info { get; set; }

        [JsonProperty("isoCode")]
        public string IsoCode { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("symbol")]
        public string Symbol { get; set; }
    }
}