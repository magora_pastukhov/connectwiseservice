using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class IndigoInfo
    {
        [JsonProperty("status_href")]
        public string StatusHref { get; set; }
    }
}