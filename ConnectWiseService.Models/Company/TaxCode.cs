using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class TaxCode
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("_info")]
        public TentacledInfo Info { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}