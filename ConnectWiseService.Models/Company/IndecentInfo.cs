using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class IndecentInfo
    {
        [JsonProperty("currency_href")]
        public string CurrencyHref { get; set; }
    }
}