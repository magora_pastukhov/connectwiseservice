using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class PurpleType
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("_info")]
        public PurpleInfo Info { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}