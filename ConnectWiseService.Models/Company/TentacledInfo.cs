using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class TentacledInfo
    {
        [JsonProperty("taxCode_href")]
        public string TaxCodeHref { get; set; }
    }
}