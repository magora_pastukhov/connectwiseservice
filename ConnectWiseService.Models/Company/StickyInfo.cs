using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class StickyInfo
    {
        [JsonProperty("company_href")]
        public string CompanyHref { get; set; }

        [JsonProperty("member_href")]
        public string MemberHref { get; set; }
    }
}