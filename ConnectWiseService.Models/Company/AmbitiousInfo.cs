using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class AmbitiousInfo
    {
        [JsonProperty("contact_href")]
        public string ContactHref { get; set; }
    }
}