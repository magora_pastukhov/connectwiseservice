using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class Status
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("_info")]
        public IndigoInfo Info { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}