using Newtonsoft.Json;

namespace ConnectWiseService.Models.Company
{
    public class CunningInfo
    {
        [JsonProperty("site_href")]
        public string SiteHref { get; set; }
    }
}