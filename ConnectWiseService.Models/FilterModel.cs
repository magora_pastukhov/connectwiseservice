﻿namespace ConnectWiseService.Models
{
    public class FilterModel
    {
        public string Conditions { get; set; } = null;
        public string OrderBy { get; set; } = null;
        public string ChildConditions { get; set; } = null;
        public string CustomFieldConditions { get; set; } = null;
        public string Page { get; set; } = null;
        public string PageSize { get; set; } = null;
    }
}