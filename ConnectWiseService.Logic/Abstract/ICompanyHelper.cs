﻿using ConnectWiseService.Models;
using ConnectWiseService.Models.Company;

namespace ConnectWiseService.Logic.Abstract
{
    public interface ICompanyHelper
    {
        Company[] GetCompanies(FilterModel filter);
    }
}