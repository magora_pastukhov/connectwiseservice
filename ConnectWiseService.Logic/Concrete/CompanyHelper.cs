﻿using System.Configuration;
using System.Net;
using ConnectWiseService.Logic.Abstract;
using ConnectWiseService.Models;
using ConnectWiseService.Models.Company;
using ConnectWiseService.Models.Helpers;
using RestSharp;

namespace ConnectWiseService.Logic.Concrete
{
    public class CompanyHelper : ICompanyHelper
    {
        public Company[] GetCompanies(FilterModel filter)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["ConnectwiseEndpoint"]);
            var request = new RestRequest(ConfigurationManager.AppSettings["ConnectwiseCompaniesResource"], Method.GET);
            request.AddHeader(ConfigurationManager.AppSettings["AuthHeaderName"], BuildBasicAuthHeader());

            if (filter != null)
            {
                if (filter.Conditions != null)
                {
                    request.AddQueryParameter("conditions", filter.Conditions);
                }
                if (filter.OrderBy != null)
                {
                    request.AddQueryParameter("orderBy", filter.OrderBy);
                }
                if (filter.ChildConditions != null)
                {
                    request.AddQueryParameter("childconditions", filter.ChildConditions);
                }
                if (filter.CustomFieldConditions != null)
                {
                    request.AddQueryParameter("customfieldconditions", filter.CustomFieldConditions);
                }
                if (filter.Page != null)
                {
                    request.AddQueryParameter("page", filter.Page);
                }
                if (filter.PageSize != null)
                {
                    request.AddQueryParameter("pageSize", filter.PageSize);
                }
            }


            var response = client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            return Company.FromJson(response.Content);
        }

        private string BuildBasicAuthHeader()
        {
            var companyName = ConfigurationManager.AppSettings["CompanyName"];
            var publicKey = ConfigurationManager.AppSettings["PublicKey"];
            var privateKey = ConfigurationManager.AppSettings["PrivateKey"];
            var base64 = StringUtils.Base64Encode($"{companyName}+{publicKey}:{privateKey}");
            return $"Basic {base64}";
        }
    }
}