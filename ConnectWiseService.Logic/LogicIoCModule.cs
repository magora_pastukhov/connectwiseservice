﻿using Autofac;
using ConnectWiseService.Logic.Abstract;
using ConnectWiseService.Logic.Concrete;

namespace ConnectWiseService.Logic
{
    public class LogicIoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<CompanyHelper>().As<ICompanyHelper>();
        }
    }
}